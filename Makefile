include common-files/base.mk
SHELL := bash

# keep-sorted start
java11-badge: docker_badge_java11
java11-push: docker_push_java11
java11-test-rmi: docker_rmi_test_java11
java11-test: docker_build_test_java11
java11-topic: docker_topic_java11
java11: docker_build_java11
# keep-sorted end
