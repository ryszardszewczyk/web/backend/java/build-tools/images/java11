FROM maven:3-jdk-11-slim

COPY common-files/java/* /usr/local/bin

RUN apt-get update \
    && apt-get -y --no-install-recommends install docker.io \
        git \
        make \
        moreutils \
        python3 \
        python3-pip \
        ssh \
        tar \
    && apt-get -y purge --auto-remove \
    && pip3 install --no-cache-dir anybadge \
    && GJF=1.19.1 \
    && TOKEI=12.1.2 \
    && curl --location --fail "https://github.com/google/google-java-format/releases/download/v${GJF}/google-java-format-${GJF}-all-deps.jar" -o /opt/google-java-format.jar \
    && curl --location --fail "https://github.com/XAMPPRocky/tokei/releases/download/v${TOKEI}/tokei-x86_64-unknown-linux-gnu.tar.gz" -o /usr/local/bin/tokei.tar.gz \
    && tar -vxzf /usr/local/bin/tokei.tar.gz \
    && apt-get -y clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* /var/tmp* \
    && rm -rf /usr/local/bin/tokei.tar.gz
